import { Component, AfterViewInit } from '@angular/core';
declare const M: any;

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements AfterViewInit {

  constructor() { }
  ngAfterViewInit() {
    const elems = document.querySelectorAll('.tabs');
    const instance = M.Tabs.init( elems, {
      swipeable: true,
      index: 3
    } );
  }

}
