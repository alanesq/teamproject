import { Component, AfterViewInit } from '@angular/core';
declare const M: any;
@Component({
  selector: 'app-collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss']
})
export class CollapsibleComponent implements AfterViewInit {

  constructor() { }

  instances: any;
  ngAfterViewInit() {
    const elems = document.querySelectorAll('.collapsible');
    this.instances = M.Collapsible.init(elems);
  }
}
