import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  data: MCard[] = [
    {
      title: "Card 1",
      body: "par utilisateur par mois, à partir de 3 utilisateurs",
      button: "Comprar",
      footer: "souscrire maintenant",
    },
    {
      title: "Card 2",
      body: "par utilisateur par mois, à partir de 3 utilisateurs",
      button: "Vender",
      footer: "souscrire maintenant",
    },
    {
      title: "Card 3",
      body: "Nous contacter",
      button: "Ir",
    },
  ];

  constructor() {}
  ngOnInit() {}
}

interface MCard {
  title: string;
  body: string;
  button: string;
  footer?: string;
  color?: string;
}
