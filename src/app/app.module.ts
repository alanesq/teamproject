import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ConocenosComponent } from './pages/conocenos/conocenos.component';
import { ContactanosComponent } from './pages/contactanos/contactanos.component';
import { APP_ROUTES } from './app.routes';
import { MainNavComponent } from './shared/main-nav/main-nav.component';
import { CardsComponent } from './shared/cards/cards.component';
import { CollapsibleComponent } from './shared/collapsible/collapsible.component';
import { ImagenComponent } from './shared/imagen/imagen.component';
import { SeguridadComponent } from './pages/seguridad/seguridad.component';
import { TabsComponent } from './shared/tabs/tabs.component';
import { IntegracionComponent } from './pages/integracion/integracion.component';
import { Cards2Component } from './shared/cards2/cards2.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ConocenosComponent,
    ContactanosComponent,
    MainNavComponent,
    CardsComponent,
    CollapsibleComponent,
    ImagenComponent,
    SeguridadComponent,
    TabsComponent,
    Cards2Component,
    IntegracionComponent,
  ],
  imports: [
    BrowserModule,
    APP_ROUTES
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
