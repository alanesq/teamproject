import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './pages/home/home.component';
import { ContactanosComponent } from './pages/contactanos/contactanos.component';
import { ConocenosComponent } from './pages/conocenos/conocenos.component';
import { SeguridadComponent } from './pages/seguridad/seguridad.component';
import { IntegracionComponent } from './pages/integracion/integracion.component';



const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'contactanos', component: ContactanosComponent },
  { path: 'conocenos', component: ConocenosComponent },
  { path: 'seguridad', component: SeguridadComponent },
  { path: 'integracion', component: IntegracionComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
   { path: '**', redirectTo: '/home' , pathMatch: 'full' }
];

export const APP_ROUTES = RouterModule.forRoot( routes, { useHash: true});
